-- MariaDB dump 10.17  Distrib 10.4.12-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: budget_back
-- ------------------------------------------------------
-- Server version 10.4.12-MariaDB-1:10.4.12+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) COLLATE utf8mb4_bin NOT NULL,
  `first_name` varchar(30) COLLATE utf8mb4_bin NOT NULL,
  `last_name` varchar(150) COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(254) COLLATE utf8mb4_bin NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `object_repr` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext COLLATE utf8mb4_bin NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `session_data` longtext COLLATE utf8mb4_bin NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `motion_apporoved`
--

DROP TABLE IF EXISTS `motion_apporoved`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `motion_apporoved` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motion_id` int(11) NOT NULL,
  `action_name` varchar(64) NOT NULL,
  `approver` varchar(64) NOT NULL,
  `memo` longtext DEFAULT NULL,
  `is_confirmed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `motion_apporoved`
--

LOCK TABLES `motion_apporoved` WRITE;
/*!40000 ALTER TABLE `motion_apporoved` DISABLE KEYS */;
INSERT INTO `motion_apporoved` VALUES (1,60,'approve','ylic','',1),(2,61,'approve','dajieshen','',1);
/*!40000 ALTER TABLE `motion_apporoved` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `motions`
--

DROP TABLE IF EXISTS `motions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `motions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motion_id` int(11) NOT NULL,
  `root_id` int(11) NOT NULL,
  `title` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `proposer` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `section` int(11) NOT NULL,
  `takecoin_num` int(11) NOT NULL,
  `approve_end_block_num` int(11) NOT NULL,
  `extern_data` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `is_confirmed` tinyint(1) NOT NULL,
  `trx_id` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `approved` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `requested` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `unapproved` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `motions_motion_id_approve_end_block_num_66ff6179_uniq` (`motion_id`,`approve_end_block_num`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `motions`
--

LOCK TABLES `motions` WRITE;
/*!40000 ALTER TABLE `motions` DISABLE KEYS */;
INSERT INTO `motions` VALUES (1,0,0,'About the Budget Application of EOSForce BP Management Platform DAPP(关于《EOSForce BP管理平台DAPP》预算申请)','100000.0000 EOS','eosawake',1,0,14065274,'[]',1,'','legendyi,ylic,damao,eosc33eosc33','oneway','','#### 1. General Project Information(项目基本信息)\n**Project Name:** EOSForce BP Management Platform\n**Description and Goal Statement:** The BP management platform will integrate BP\'s daily operations, enhance BP\'s ability to collaborate, and show the community the progress of BP\'s work. Promote the ecological development of EOSC.\n**Website:** https://api.akdex.io/bpmsig\n\n**项目名称：** EOSForce BP管理平台\n**项目描述：** BP管理平台将集成BP的日常操作，提升BP相互协作的能力，向社区展示BP工作的进度。促进EOSC生态发展。\n**项目网址：** https://api.akdex.io/bpmsig\n\n#### 2. Project Team(项目开发团队)\n**Team Name：** eosawake Nodes in the community\n**Team Introduce：** Eosawake community is one of the earliest nodes to participate in the governance of EOSC main network. In terms of EOSC ecology, there are the development of Akdex decentralized exchange and Awake decentralized wallet.\n**Contact Information：** https://t.me/eosawake,service@akdex.io\n\n**团队名称：** eosawake节点社区\n**团队简介：** eosawake社区是最早参与EOSC主网治理的节点之一，在EOSC生态上有开发Akdex去中心化交易所，Awake去中心化钱包。\n**团队联系：** https://t.me/eosawake,service@akdex.io\n\n#### 3. Project Scope Statement(项目范围)：\n\n**Project Demand(项目需求)：** \nBecause BP\'s daily work needs to use the private key of BP account, the current method can only be operated in the command line environment, which is very inconvenient. BP management platform can solve this demand, so that BP daily operation does not need a computer, and can be completed at any time anywhere on the mobile phone wallet related operations. And the results of the operation, the whole community users can see.\n\n由于BP的日常工作需要用到BP账户私钥，现行的办法只能在命令行环境下进行操作，此种操作十分不便。BP管理平台可以解决这一需求，使BP日常操作无需电脑，可以随时随地在手机钱包上即可完成相关的操作。并且操作结果，全社区用户都可以查看。\n\n**Project Objectives(项目目标)：**\n\n1) Display board for current node status\n2) Relevant data display of block-producing nodes and standby nodes, such as produced blocks, voting weight, missed blocks, penalty, reward, etc.\n3) Relevant operations of nodes, such as block-missing record, increasing deposit, claiming reward, proposing penalty nodes, etc.\n4) Multi-sig module to display the current list for nodes to execute multi-sig and provide the operational button for multi-sig.\n5) Node information filling and modification module for nodes to use bpinfo contract conveniently.\n\n1)显示当前节点状态看板。\n2)出块节点和备用节点相关数据显示，如出块数、票权、漏块数、罚金、押金、收益等。\n3)各节点相关的操作，如漏块记录、增加押金、领取收益、提议惩罚节点、同意惩罚节点等。\n4)节点多签模块，显示当前需要BP多签的列表，提供多签操作按扭。\n5)节点信息填写和修改模块，方便节点使用bpinfo合约。\n\n#### 4. Project Milestones and Schedule(项目进度表)\n\n2019-10-01  Project establishment(立项)\n2019-10-02  Project demand analysis(项目需求分析)\n2019-10-05  Project framework design(项目架构设计)\n2019-10-08  Project starts coding(项目开始编码)\n2019-10-12  Internal Test(内测)\n2019-10-15  Open Beta(公测)\n2019-10-20  Official Launch(正式上线)',0),(2,1,0,'中心化预算系统开发费用申请','550000.0000 EOS','legendyi',1,0,14091101,'[]',1,'','ylic,eosc33eosc33,damao,oneway','legendyi','','项目名称：中心化预算系统开发费用申请\n\nProject Name: Centralized Budget System Development Expense Application\n\n项目描述：\n\n经过《EOSFORCE主网改进提案V1.0FIP #7》会议内容，启动预算委员会来进行管理和组织生态应用开发。为此EOSFroce团队组织并开发了去中心化预算系统页面。目前已经开发完毕，并正式上线上线，同时在官方网页上添加入口，用户可以直接在官网跳转至预算系统网站。\n\nDescription：\nAccording to the content of the EOSFORCE Mainnet Improvement Proposal V1.0 FIP #7, the budget committee was started to manage and organize the ecological application development. To this end, the EOSFroce team organized and developed a decentralized budget system page. It has been developed and officially launched online, and at the same time add an entry on the official website, users can jump directly to the budget system website on the official website.\n\n项目功能\n预算系统所有数据将在链上同步，所有人都可以通过scatter进行登入申请提案。\n目前已经拥有功能：\n1、预选系统完整界面\n2、通过scatter合约完整操作\n3、申请版面设计\n4、各个版面的设计，包括审批中、公示中、执行中、已完成四个阶段。\n5、后续版本维护和更新。\n\nProject functions\nAll data in the budget system will be synchronized on the chain, and everyone can log in to apply for a proposal via scatter.\nExisting functions:\n1, Complete interface of pre-selected system\n2, complete operation through the scatter contract\n3. Apply for layout design\n4. The design of each layout, including the approval, the public, the implementation, and the completion of the four stages.\n5. Subsequent version maintenance and updates.\n\n项目网址：https://u.eosforce.cn/#/\nProject URL：https://u.eosforce.cn/#/\n\n开发周期：\n阶段1：产品需求（预计5个工作人日）\n1、需求整理\n2、原型设计\n阶段2：UI设计（预计5个工作人日）\n1、UI设计\n阶段3：开发及上线（预计15个工作人日）\n1、前端开发\n2、后端数据对接\n3、测试及上线\n\nDevelopment cycle:\nStage 1: Product demand (expected 5  staff days)\n1, demand finishing\n2, prototype design\nStage 2: UI design (expected 5 staff days)\n1, UI design\nStage 3: Development and launch (expected 15 working days)\n1, front-end development\n2, back-end data docking\n3, test and go online\n\n开发人员：\nbandon（开发）、hubery（设计）、lex（产品）\nDevelopers:\nBandon (development), hubery (design), lex (product management)\n\n开发费用：\n工时：25个工作人日\n合计：500000 eosc\n服务器费用一年：30000 eosc\n维护费用一年：20000eosc\n总计：550000eosc\n\nDevelopment costs:\n\nWorking hours: 25 working days\nTotal: 500000 eosc\nServer fee for one year: 30,000 eosc\nMaintenance cost for one year: 20000eosc\n\nTotal: 550,000eosc\n',0),(3,2,0,'Decentralized Budget System','550000.0000 EOS','forcecore',1,0,14124885,'[]',1,'','legendyi,oneway,ylic,damao','eosc33eosc33','','# 项目名称：去中心化预算系统\n\n# Project Name: Decentralized Budget System \n\n## 项目描述：\n经过《 EOSFORCE主网改进提案V1.0FIP #7》会议内容，启动预算委员会来进行管理和组织生态应用开发。为此EOSFroce团队组织并开发了去中心化预算系统页面。目前已经开发完毕，并正式上线上线，同时在官方网页上添加入口，用户可以直接在官网跳转至预算系统网站。\n\n## Description： \nAccording to the content of the EOSFORCE Mainnet Improvement Proposal V1.0 FIP #7, the budget committee was started to manage and organize the ecological application development. To this end, the EOSFroce team organized and developed a decentralized budget system page. It has been developed and officially launched online, and at the same time add an entry on the official website, users can jump directly to the budget system website on the official website.\n\n## 项目功能\n预算系统所有数据将在链上同步，所有人都可以通过scatter进行登入申请提案。 目前已经拥有功能：\n  \n      1、预选系统完整界面 \n      2、通过scatter合约完整操作 \n      3、申请版面设计 \n      4、各个版面的设计，包括审批中、公示中、执行中、已完成四个阶段。 \n      5、后续版本维护和更新。\n\n\n## Project functions\nAll data in the budget system will be synchronized on the chain, and everyone can log in to apply for a proposal via scatter. Existing functions:\n \n \n``` 1、Complete interface of pre-selected system \n  2、complete operation through the scatter contract \n  3、Apply for layout design \n  4、 The design of each layout, including the approval, the public, the implementation, and the completion of the four stages. \n  5、 Subsequent version maintenance and updates.\n\n```\n\n\n## 项目网址\nhttps://u.eosforce.cn/#/ \n\n## Project URL\nhttps://u.eosforce.cn/#/\n\n## 开发周期\n* 阶段1：产品需求（预计5个工作人日） \n        1、需求整理 \n        2、原型设计 \n* 阶段2：UI设计（预计5个工作人日） \n        1、UI设计 \n* 阶段3：开发及上线（预计15个工作人日） \n        1、前端开发 \n        2、后端数据对接 \n        3、测试及上线\n        \n## Development cycle\n* Stage 1: Product demand (expected 5 staff days) \n    1、 demand finishing \n    2、 prototype design Stage \n* Stage 2: UI design (expected 5 staff days) \n    1、UI design \n* Stage 3: Development and launch (expected 15 working days)\n    1、front-end development \n    2、back-end data docking \n    3、test and go online\n\n## 开发费用\n* 工时：25个工作人日  500000 eosc \n* 服务器费用一年：30000 eosc \n* 维护费用一年：20000 eosc \n* 总计：550000 eosc\n\n## Development costs\n* Working hours: 25 working days   500000 eosc \n* Server fee for one year: 30,000 eosc \n* Maintenance cost for one year: 20000 eosc\n* Total: 550,000eosc',0),(4,3,0,'TestTitle','101.0000 EOS','test',2,0,14208329,'[]',1,'','','ylic,damao,eosc33eosc33','legendyi,oneway','TestDescription',0),(5,4,0,'eosc原力的技术和特点','20000.0000 EOS','eostozhemoon',2,0,14379453,'[]',1,'','ylic,legendyi','eosc33eosc33','damao,oneway','##### 1、项目名称： 原力技术特性宣传文章\n\n##### 2、项目描述：\n在币乎平台写文章宣传EOSc，并将清楚他的技术特点，优势和详细介绍\n\n##### 3、项目功能：\n主要从以下几点进行写作：\n\n1、更新机制和节点心跳机制\n2、基于时间加权的投票机制\n3、去中心化预算系统\n4、高吞吐低延迟共识机制\n5、高度可定制化的EOSIO区块链开发框架\n\n1000字数左右，详细讲清楚eosc原力是什么，有什么创新和优势。\n\n\n#####  预期效果\n通过币乎平台社区流量进行转化。目前币乎日活用户高达2W。\n文章保证留言数300+，阅读数4000+，预期关注人数500+\n\n##### 7、费用\n\n文章撰写+平台发布+社群同步\n\n\n##### 发布时间\n\n2019年11月5号或6号。发布后将在各个社群同步分享。',0),(6,5,0,'EOSC社群主题分享活动第一期','11000.0000 EOS','soupokmengsb',0,0,14488272,'[]',1,'','','oneway,ylic,damao,eosc33eosc33,legendyi','','对当下社区讨论热点，邀请了以下11位群友分享相关主题。分享方式：微信群。目标：分享和讨论结合，对当下群友关心话题充份讨论，就算有分岐也能减少社群共识难度。奖励每个分享者1000个eosc。\n以下是11个主题分享内容\n\n舍得分享主题：10人的宣传组功能定义\n\n易sir分享主题：预算案的基础费用+效果奖励解释.\n\noneway分享主题：6人的宣传部详细分工\n\n陈梅升分享主题：eosforce稳定币探讨\n\nMr阿军分享主题：建立eosforce联盟统一品牌\n\n一枝毒秀分享主題：eosforce参与公共事务区块链建设\n\n富春分享主題：eosforce免费币处理方法\n\n無名分享主題：eosc怎样才能成为良币\n\n海鴎分享主題: 部门多了不是好事\n\n藍分享主題: 技术性宣传文章如何写\n\n夏天分享主題: 如何吸引人参与到社区',0),(7,6,0,'EOSC社群主题分享活动第一期','11000.0000 EOS','soupokmengsb',1,0,14488590,'[]',1,'','legendyi,oneway,damao,ylic','eosc33eosc33','','对当下社区讨论热点，邀请了以下11位群友分享相关主题。分享方式：微信群。目标：分享和讨论结合，对当下群友关心话题充份讨论，就算有分岐也能减少社群共识难度。奖励每个分享者1000个eosc。\n以下是11个主题分享内容\n\n舍得分享主题：10人的宣传组功能定义\n\n易sir分享主题：预算案的基础费用+效果奖励解释.\n\noneway分享主题：6人的宣传部详细分工\n\n陈梅升分享主题：eosforce稳定币探讨\n\nMr阿军分享主题：建立eosforce联盟统一品牌\n\n一枝毒秀分享主題：eosforce参与公共事务区块链建设\n\n富春分享主題：eosforce免费币处理方法\n\n無名分享主題：eosc怎样才能成为良币\n\n海鴎分享主題: 部门多了不是好事\n\n藍分享主題: 技术性宣传文章如何写\n\n夏天分享主題: 如何吸引人参与到社区',0),(8,7,0,'关于成立基于eosforce的区块链企业联盟提案（以下简称“原力区块链企业联盟”）','21000.0000 EOS','ajun123',0,0,14496516,'[]',1,'','','oneway,ylic,damao,eosc33eosc33,legendyi','','一、项目名称：关于成立基于eosforce的区块链企业联盟提案（以下简称“原力区块链企业联盟”）\n二、项目描述：本提案介绍了基于原力技术，由原力团队主导、主要社区成员企业参与，共同建立区块链企业联盟的设计方案，对此方案的具体实施提供一些思路、建议并阐述所列举的合作案例供社区成员进行讨论，最后提交至原力公司团队内部、原力社区治理委员会、预算管理委员会以及原力社区进行最终的决策。\n三、项目细节：\n1、摘要：\n本提案介绍了基于原力技术，由原力团队主导、主要社区成员企业参与，共同建立区块链企业联盟的设计方案，对此方案的具体实施提供一些思路、建议并阐述所列举的合作案例供社区成员进行讨论，最后提交至原力公司团队内部、原力社区治理委员会、预算管理委员会以及原力社区进行最终的决策。\n2、引言：\n习近平主席在中共中央政治局第十八次集体学习时强调，把区块链技术作为核心技术自主创新的重要突破口，他指出，区块链技术应用已延伸到数字金融、物联网、智能制造、供应链管理、数字资产交易等多个领域。目前全球主要国家都在加快区块链技术发展。我国在区块链领域拥有良好基础，要加快推动区块链技术和产业创新发展和经济社会融合发展。因此，面对国家层面的认可，未来可能爆发的产业及政策红利，我们提出了关于成立“原力区块链企业联盟”的提案。\n3、原力（eosforce）的介绍\n原力的团队、技术及社区\n3.1 原力的团队\n原力主网核心开发团队—EOS原力团队，全部由中国的资深区块链开发者组成，团队创始人孤矢来自BCH社区，当初启动原力主网便得到众多BCH社区成员的支持，其余联合创始人均是国内资深区块链技术开发者，团队创新能力和开发能力极强，并且原力团队为目前国内公链中唯一未进行过ICO或者任何代币募资团队。\n3.2 原力技术\nEOSFORCE是中国原力团队早EOS主网启动时，启动的另外一条公链，2018年6月主网上线，比EOS主网还要早1周时间启动。原力团队对EOS设计了不同的治理机制，在创世高度1启动了EOSC主网，并持续对EOSC主网进行迭代升级，使得EOSC朝着去中心化的高性能智能合约平台的方向持续演进，并具有以下创新点：\n  大规模Staking开创者\n  高度可定制化的区块链开发框架\n 时间加权的一票一权投票机制\n  首个POS链上去中心化预算系统\n3.3 原力社区\n目前原力社区运营全部由社区节点自发组织推进，为EOSC主网设计了完备的治理机制和预算系统，未来即使开发团队离开社区，社区也能通过自组织系统实现技术迭代和社区正常运转，社区忠诚度和活跃度都很高。在这样的社区中，很多节点或个人拥有实体公司，均为区块链技术服务商，这样技术的积累和市场的把握，将会给联盟带来巨大的能量。\n4、原力区块链企业联盟介绍\n4.1 原力区块链企业联盟组成\n联盟是基于EOSFORCE，由杭州柚子原力团队主导，社区成员企业参与建设的组织机构。\n4.2 原力区块链企业联盟目的\n联盟成员皆为实体公司组织，他们可以在政策支持及响应下，使用原力区块链技术，在各自所在的地区就政务开发、商务伙伴及自身平台建设上发挥作用，既能获取收益，又可以拓展原力技术的应用场景，丰富原力生态的建设，让原力走进公众的视野，成为国内顶级技术公链。\n4.3 原力区块链企业联盟具体实施\n1) 杭州柚子原力公司作为发起单位，社区成员（拥有实体公司）积极响应加入，并成立原力区块链企业联盟委员会，委员会由各个企业代表选举组成，参与联盟日常规范管理，处理业务合作中的各项问题，首任委员会主席由原力公司派出代表担任。\n2)  制定《联盟业务合作发展细节规范》，用于各成员在业务合作与发展过程中，使用原力技术，进行项目开发过程中的制度规范。包括：技术支持形式、利润分配细节、业务洽谈规范等方面。（可由原力上午宣传部主导草拟，联盟成员进行补充或就一些具体问题进行修改与讨论。）\n3) 联盟成立及《联盟业务合作发展细节规范》出台后，各成员企业便可在各自业务范围区域进行业务谈判，有资源的可进行政务订单开发，大型零售企业的供应链系统等。\n4.4 案例列举分析\n人社局对人才及各类人员培训专项补贴发放系统：联盟成员在拿到政府订单后，需要使用区块链技术进行开发，但自身无法承担这部分的开发任务，即可上报联盟委员会，并填写申请表，审批通过后即可根据《联盟业务合作发展细节规范》来执行。\n例如：\n甲在A市，拿到A市的专项资金分发使用系统开发的订单后，甲需要使用原力技术完成开发，便上报了委员会，受理后，填写了申请表，审批通过，按照《联盟业务合作发展细节规范》签署相关协议，共同完成开发任务。最终开发获取的荣誉与经济利益，按照《规范》严格执行。\n5、原力区块链企业联盟可能存在的问题\n1) 原力区块链企业联盟成员水平参差不齐，具体落地实施过程中，可能遇到很多开发问题，因此制定好《联盟业务合作发展细节规范》很有必要。\n2) 沟通成本无论是什么企业都需要面对的，希望各方可以平心静气共同完成任务目标。\n3) 商务推广上，各方付出的努力不尽相同，如何能够让原力区块链企业联盟的品牌效应产生溢价，设立新的Brand很有必要。\n6、总结\n针对国家层面的高度重视区块链技术的应用发展，作为原力社区的成员，我们有责任和义务，推动原力技术的落地实施，丰富原力的生态建设，加强原力技术推广、态的建设、品牌的溢价，也将进一步推动原力进入国产公链的第一梯队行列，同时eosc的币价也会得到反哺。原力区块链企业联盟应国家政策响应而生，是符合时代主旋律的，也必将成功。\n',0),(9,7,0,'关于成立基于eosforce的区块链企业联盟提案（以下简称“原力区块链企业联盟”）','21000.0000 EOS','ajun123',0,0,14497073,'[]',1,'','','oneway,ylic,damao,eosc33eosc33,legendyi','','一、项目名称：关于成立基于eosforce的区块链企业联盟提案（以下简称“原力区块链企业联盟”）\n\n二、项目描述：本提案介绍了基于原力技术，由原力团队主导、主要社区成员企业参与，共同建立区块链企业联盟的设计方案，对此方案的具体实施提供一些思路、建议并阐述所列举的合作案例供社区成员进行讨论，最后提交至原力公司团队内部、原力社区治理委员会、预算管理委员会以及原力社区进行最终的决策。\n\n三、项目细节：\n\n1、摘要：\n本提案介绍了基于原力技术，由原力团队主导、主要社区成员企业参与，共同建立区块链企业联盟的设计方案，对此方案的具体实施提供一些思路、建议并阐述所列举的合作案例供社区成员进行讨论，最后提交至原力公司团队内部、原力社区治理委员会、预算管理委员会以及原力社区进行最终的决策。\n\n2、引言：\n习近平主席在中共中央政治局第十八次集体学习时强调，把区块链技术作为核心技术自主创新的重要突破口，他指出，区块链技术应用已延伸到数字金融、物联网、智能制造、供应链管理、数字资产交易等多个领域。目前全球主要国家都在加快区块链技术发展。我国在区块链领域拥有良好基础，要加快推动区块链技术和产业创新发展和经济社会融合发展。因此，面对国家层面的认可，未来可能爆发的产业及政策红利，我们提出了关于成立“原力区块链企业联盟”的提案。\n\n3、原力（eosforce）的介绍\n\n原力的团队、技术及社区\n\n3.1 原力的团队\n：原力主网核心开发团队—EOS原力团队，全部由中国的资深区块链开发者组成，团队创始人孤矢来自BCH社区，当初启动原力主网便得到众多BCH社区成员的支持，其余联合创始人均是国内资深区块链技术开发者，团队创新能力和开发能力极强，并且原力团队为目前国内公链中唯一未进行过ICO或者任何代币募资团队。\n\n3.2 原力技术：EOSFORCE是中国原力团队早EOS主网启动时，启动的另外一条公链，2018年6月主网上线，比EOS主网还要早1周时间启动。原力团队对EOS设计了不同的治理机制，在创世高度1启动了EOSC主网，并持续对EOSC主网进行迭代升级，使得EOSC朝着去中心化的高性能智能合约平台的方向持续演进，并具有以下创新点：\n大规模Staking开创者、高度可定制化的区块链开发框架、时间加权的一票一权投票机制、首个POS链上去中心化预算系统。\n\n\n3.3 原力社区：目前原力社区运营全部由社区节点自发组织推进，为EOSC主网设计了完备的治理机制和预算系统，未来即使开发团队离开社区，社区也能通过自组织系统实现技术迭代和社区正常运转，社区忠诚度和活跃度都很高。在这样的社区中，很多节点或个人拥有实体公司，均为区块链技术服务商，这样技术的积累和市场的把握，将会给联盟带来巨大的能量。\n\n4、原力区块链企业联盟介绍\n\n4.1 原力区块链企业联盟组成：联盟是基于EOSFORCE，由杭州柚子原力团队主导，社区成员企业参与建设的组织机构。\n\n4.2 原力区块链企业联盟目的：联盟成员皆为实体公司组织，他们可以在政策支持及响应下，使用原力区块链技术，在各自所在的地区就政务开发、商务伙伴及自身平台建设上发挥作用，既能获取收益，又可以拓展原力技术的应用场景，丰富原力生态的建设，让原力走进公众的视野，成为国内顶级技术公链。\n\n4.3 原力区块链企业联盟具体实施：\n\n1)  杭州柚子原力公司作为发起单位，社区成员（拥有实体公司）积极响应加入，并成立原力区块链企业联盟委员会，委员会由各个企业代表选举组成，参与联盟日常规范管理，处理业务合作中的各项问题，首任委员会主席由原力公司派出代表担任。\n\n2)  制定《联盟业务合作发展细节规范》，用于各成员在业务合作与发展过程中，使用原力技术，进行项目开发过程中的制度规范。包括：技术支持形式、利润分配细节、业务洽谈规范等方面。（可由原力上午宣传部主导草拟，联盟成员进行补充或就一些具体问题进行修改与讨论。）\n\n3) 联盟成立及《联盟业务合作发展细节规范》出台后，各成员企业便可在各自业务范围区域进行业务谈判，有资源的可进行政务订单开发，大型零售企业的供应链系统等。\n\n4.4 案例列举分析：人社局对人才及各类人员培训专项补贴发放系统：联盟成员在拿到政府订单后，需要使用区块链技术进行开发，但自身无法承担这部分的开发任务，即可上报联盟委员会，并填写申请表，审批通过后即可根据《联盟业务合作发展细节规范》来执行。\n\n例如：\n\n甲在A市，拿到A市的专项资金分发使用系统开发的订单后，甲需要使用原力技术完成开发，便上报了委员会，受理后，填写了申请表，审批通过，按照《联盟业务合作发展细节规范》签署相关协议，共同完成开发任务。最终开发获取的荣誉与经济利益，按照《规范》严格执行。\n\n5、原力区块链企业联盟可能存在的问题\n\n1)  原力区块链企业联盟成员水平参差不齐，具体落地实施过程中，可能遇到很多开发问题，因此制定好《联盟业务合作发展细节规范》很有必要。\n\n2) 沟通成本无论是什么企业都需要面对的，希望各方可以平心静气共同完成任务目标。\n\n3) 商务推广上，各方付出的努力不尽相同，但又目标一致，就是通过原力技术的推广应用，获取来自政策及社会发展变革的红利，如何能够获取更大的效益，让原力区块链企业联盟的品牌效应产生溢价，设立新的Brand很有必要。\n\n6、总结\n\n针对国家层面的高度重视区块链技术的应用发展，作为原力社区的成员，我们有责任和义务，推动原力技术的落地实施，丰富原力的生态建设，加强原力技术推广、态的建设、品牌的溢价，也将进一步推动原力进入国产公链的第一梯队行列，同时eosc的币价也会得到反哺。原力区块链企业联盟应国家政策响应而生，是符合时代主旋律的，也必将成功。\n\n',0),(10,7,0,'关于成立基于eosforce的区块链企业联盟提案（以下简称“原力区块链企业联盟”）','21000.0000 EOS','ajun123',1,0,14497245,'[]',0,'','ylic,damao,legendyi,oneway','eosc33eosc33','','一、项目名称：关于成立基于eosforce的区块链企业联盟提案（以下简称“原力区块链企业联盟”）\n\n二、项目描述：本提案介绍了基于原力技术，由原力团队主导、主要社区成员企业参与，共同建立区块链企业联盟的设计方案，对此方案的具体实施提供一些思路、建议并阐述所列举的合作案例供社区成员进行讨论，最后提交至原力公司团队内部、原力社区治理委员会、预算管理委员会以及原力社区进行最终的决策。\n\n三、项目细节：\n\n1、摘要：\n本提案介绍了基于原力技术，由原力团队主导、主要社区成员企业参与，共同建立区块链企业联盟的设计方案，对此方案的具体实施提供一些思路、建议并阐述所列举的合作案例供社区成员进行讨论，最后提交至原力公司团队内部、原力社区治理委员会、预算管理委员会以及原力社区进行最终的决策。\n\n2、引言：\n习近平主席在中共中央政治局第十八次集体学习时强调，把区块链技术作为核心技术自主创新的重要突破口，他指出，区块链技术应用已延伸到数字金融、物联网、智能制造、供应链管理、数字资产交易等多个领域。目前全球主要国家都在加快区块链技术发展。我国在区块链领域拥有良好基础，要加快推动区块链技术和产业创新发展和经济社会融合发展。因此，面对国家层面的认可，未来可能爆发的产业及政策红利，我们提出了关于成立“原力区块链企业联盟”的提案。\n\n3、原力（eosforce）的介绍\n\n原力的团队、技术及社区\n\n3.1 原力的团队\n：原力主网核心开发团队—EOS原力团队，全部由中国的资深区块链开发者组成，团队创始人孤矢来自BCH社区，当初启动原力主网便得到众多BCH社区成员的支持，其余联合创始人均是国内资深区块链技术开发者，团队创新能力和开发能力极强，并且原力团队为目前国内公链中唯一未进行过ICO或者任何代币募资团队。\n\n3.2 原力技术：EOSFORCE是中国原力团队早EOS主网启动时，启动的另外一条公链，2018年6月主网上线，比EOS主网还要早1周时间启动。原力团队对EOS设计了不同的治理机制，在创世高度1启动了EOSC主网，并持续对EOSC主网进行迭代升级，使得EOSC朝着去中心化的高性能智能合约平台的方向持续演进，并具有以下创新点：\n大规模Staking开创者、高度可定制化的区块链开发框架、时间加权的一票一权投票机制、首个POS链上去中心化预算系统。\n\n\n3.3 原力社区：目前原力社区运营全部由社区节点自发组织推进，为EOSC主网设计了完备的治理机制和预算系统，未来即使开发团队离开社区，社区也能通过自组织系统实现技术迭代和社区正常运转，社区忠诚度和活跃度都很高。在这样的社区中，很多节点或个人拥有实体公司，均为区块链技术服务商，这样技术的积累和市场的把握，将会给联盟带来巨大的能量。\n\n4、原力区块链企业联盟介绍\n\n4.1 原力区块链企业联盟组成：联盟是基于EOSFORCE，由杭州柚子原力团队主导，社区成员企业参与建设的组织机构。\n\n4.2 原力区块链企业联盟目的：联盟成员皆为实体公司组织，他们可以在政策支持及响应下，使用原力区块链技术，在各自所在的地区就政务开发、商务伙伴及自身平台建设上发挥作用，既能获取收益，又可以拓展原力技术的应用场景，丰富原力生态的建设，让原力走进公众的视野，成为国内顶级技术公链。\n\n4.3 原力区块链企业联盟具体实施：\n\n1)  杭州柚子原力公司作为发起单位，社区成员（拥有实体公司）积极响应加入，并成立原力区块链企业联盟委员会，委员会由各个企业代表选举组成，参与联盟日常规范管理，处理业务合作中的各项问题，首任委员会主席由原力公司派出代表担任。\n\n2)  制定《联盟业务合作发展细节规范》，用于各成员在业务合作与发展过程中，使用原力技术，进行项目开发过程中的制度规范。包括：技术支持形式、利润分配细节、业务洽谈规范等方面。（可由原力上午宣传部主导草拟，联盟成员进行补充或就一些具体问题进行修改与讨论。）\n\n3) 联盟成立及《联盟业务合作发展细节规范》出台后，各成员企业便可在各自业务范围区域进行业务谈判，有资源的可进行政务订单开发，大型零售企业的供应链系统等。\n\n4.4 案例列举分析：人社局对人才及各类人员培训专项补贴发放系统：联盟成员在拿到政府订单后，需要使用区块链技术进行开发，但自身无法承担这部分的开发任务，即可上报联盟委员会，并填写申请表，审批通过后即可根据《联盟业务合作发展细节规范》来执行。\n\n例如：\n\n甲在A市，拿到A市的专项资金分发使用系统开发的订单后，甲需要使用原力技术完成开发，便上报了委员会，受理后，填写了申请表，审批通过，按照《联盟业务合作发展细节规范》签署相关协议，共同完成开发任务。最终开发获取的荣誉与经济利益，按照《规范》严格执行。\n\n5、原力区块链企业联盟可能存在的问题\n\n1)  原力区块链企业联盟成员水平参差不齐，具体落地实施过程中，可能遇到很多开发问题，因此制定好《联盟业务合作发展细节规范》很有必要。\n\n2) 沟通成本无论是什么企业都需要面对的，希望各方可以平心静气共同完成任务目标。\n\n3) 商务推广上，各方付出的努力不尽相同，但又目标一致，就是通过原力技术的推广应用，获取来自政策及社会发展变革的红利，如何能够获取更大的效益，让原力区块链企业联盟的品牌效应产生溢价，设立新的Brand很有必要。\n\n6、总结\n\n针对国家层面高度重视区块链技术的应用发展，作为原力社区的成员，我们有责任和义务，推动原力技术的落地实施，丰富原力的生态建设，加强原力技术推广、生态的建设、品牌的溢价，也将进一步推动原力进入国产公链的第一梯队行列，同时eosc的币价也会得到反哺。原力区块链企业联盟应国家政策响应而生，是符合时代主旋律的，也必将成功。\n\n',0),(11,8,0,'eosc共振eos','5100000.0000 EOS','eoscgo',2,0,14546055,'[]',1,'','','oneway,eosc33eosc33,legendyi','ylic,damao','### EOSC共振EOS\n#### 前言：\n随着EIDOS的火爆，CPU挖矿迎来了一个新纪元。目前EIDOS虽然无任何价值，但是其风靡程度依旧不减，令EOS主网瘫痪。不过有用户的市场就是好市场。\n受EISOS启发。我代表EOSC开发**《EOSC共振EOS》**项目。来进一步获取EOS用户流量和关注，使EOSC达到和EOS共振的目的。\n\n#### 规则：\n设置智能合约eosforcecoin账户，用户通过将EOS转账至eosforcecoin这个合约账户，智能合约将等量的 EOS返回至用户账户，并会将eosforcecoin 这个账户中存有的 0.01% EIDOS 发送到用户账户中。 单次挖矿最高转账100EOS，每一秒内会有 20 个 EOSC 产生，每24小时产量减半。挖完为止。\n第一轮空投共计投出5000000EOSC，简单来说就是不停给 eidosonecoin 这个账号转账，每次转账消耗 CPU 资源来获取账号内万分之一的 EOSC 。\n',0),(12,9,0,'通过币圈大V推广EOSC的提案申请','30000.0000 EOS','gu2dsmztguge',2,0,14671525,'[]',1,'','','oneway,eosc33eosc33,legendyi','damao,ylic','拟邀请币圈大V金马推广eosc，描述如下：\n1、由eos原力社区委员会提供相关eosc的宣传素材，委托大V撰写eosc的宣传文章（软文）\n2、由eos原力社区委员会评审文章内容，评审通过后由大V通过个人自媒体渠道进行宣传\n3、大V主要个人自媒体渠道如下（通过以下全部渠道进行发布）\nA、币乎：粉丝数60000+\nB、微信群：8个收费群，2个超级会员群\nC、简书：粉丝数6000+\nD、知乎：关注数690+\nE、微博+今日头条等\n4、宣传文章发布后由原力社区委员会评估大V宣传结果',0),(13,10,0,'通过币圈大V推广EOSC的提案申请','30000.0000 EOS','fubusikong',2,0,14714810,'[]',1,'','','oneway,eosc33eosc33,legendyi','damao,ylic','\n拟邀请币圈大V金马推广eosc，描述如下：\n1、由eos原力社区委员会提供相关eosc的宣传素材，委托大V撰写eosc的宣传文章（软文）\n2、由eos原力社区委员会评审文章内容，评审通过后由大V通过个人自媒体渠道进行宣传\n3、大V主要个人自媒体渠道如下（通过以下全部渠道进行发布）\nA、币乎：粉丝数60000+\nB、微信群：8个收费群，2个超级会员群\nC、简书：粉丝数6000+\nD、知乎：关注数690+\nE、微博+今日头条等\n4、宣传文章发布后由原力社区委员会评估大V宣传结果',0),(14,11,0,'通过金马写文章推广更多人了解 EOSC 的提案申请','30000.0000 EOS','sheldon',0,0,14893146,'[]',1,'','ylic,legendyi,damao','oneway,eosc33eosc33','','1、项目名称：邀请金马写文宣传 EOSC\n2、项目描述：\n\n1）、由eos原力社区委员会提供相关eosc的宣传素材，委托作者撰写eosc的宣传文章\n2）、由eos原力社区委员会评审文章内容，评审通过后由作者通过个人自媒体渠道进行宣传\n3）、作者主要个人自媒体渠道如下（通过以下全部渠道进行发布） A、币乎：粉丝数60000+ B、微信群： 8 个免费群，8个收费群，2个超级会员群 C、简书专栏：粉丝数 6000+ D、知乎：关注数690+ E、微博+力场等\n4）、宣传文章发布后由原力社区委员会评估作者宣传结果\n\n3、项目功能：\n\n让更多 EOS 用户了解 EOSC 的价值和优势。\n\n4、项目链接：\n\n币乎文章链接：https://bihu.com/article/1409602260  （这个是主要的内容平台）\n简书链接：https://www.jianshu.com/p/bdb7928ed65f\n知乎链接：https://zhuanlan.zhihu.com/p/92144585\n微博链接：https://weibo.com/ttarticle/p/show?id=2309404439351227449578#_0\n力场链接：https://lichang.io/articleDetail/963667',0),(15,12,0,'社区委员会代表EOSC，为Bibix2周年打call','100000.0000 EOS','legendyi',1,0,14917344,'[]',1,'','legendyi,damao,ylic,oneway','eosc33eosc33','','### 宣传渠道\n#### 1、大佬打call，海报图附上红包领取入口 业内  大佬、媒体、项目、社区KOL等 吴忌寒、沈波、比特币女博士、区块链威廉等  \n  业外  传统金融机构高管、财经媒体主编等  Aviva集团（捷克）CEO、VISA前CEO等  \n#### 2、垂直媒体上发布红包Banner  垂直媒体  金色、币快报、Mytoken、Aicoin 媒体焦点图/弹窗/开机屏/社区等\n#### 3、财经媒体宣发 媒体  媒体、Bibox自媒体渠道 项目曝光\n#### 4、快讯发布 媒体  媒体、Bibox自媒体渠道 项目曝光  \n#### 5、社群&媒体社区  Bibox及50+项目 中、日、英、韩、俄、土耳其、法、德等多语言社群、媒体  微信、电报、kakaotalk、Line、垂直媒体社群 \n#### 6、官方中文自媒体  Bibox及50+项目微博 Bibox相关微博及项目相关微博  \n#### 7、官方英文自媒体  Bibox及50+项目 twitter、Facebook、medium等  项目相关国内外宣传渠道 \n#### 8、外部社群社区外部合作社区 社区内部及相关渠道 小巴社区、海思社区、小猪社区、币橙社区、牛牛社区、白一社区、大圣资本等 ',0),(16,13,0,'Go Block','400000.0000 EOS','wangheran123',0,0,15175033,'[]',1,'','ylic,eosc33eosc33','oneway,damao,legendyi','','《GoBlock》是DAppBirds即将基于EOSC发行的一款非常好玩的休闲射击游戏，你可以选择武器、升级武器，灭除各种各样的讨厌的病毒区块，还能每天冲榜瓜分高额榜单奖励。',0),(17,14,0,'生态游戏《Go Block》 ','0.0000 EOS','wangheran123',1,0,15176329,'[]',0,'','legendyi,damao,ylic,oneway','eosc33eosc33','','项目名称：《GoBlock》\n\n项目描述：《GoBlock》是DAppBirds基于eosc发行的一款非常好玩的休闲射击游戏，你可以选择武器、升级武器，灭除各种各样的讨厌的病毒区块，还能每天冲榜瓜分高额榜单奖励。\n\n项目功能：给eosc生态用户更好的体验，同时实现流通eosc\n\n开发人员：共计5人，其中开发3人，美术1人，产品1人\n\n开发周期：前后端程序、美术、产品（预计10个工作日） \n\n开发费用：240000EOSC  服务器维护费用（50000）人工成本（190000）\n\n\n',0),(18,15,0,'通过金马写文章推广更多人了解 EOSC 的提案申请','30000.0000 EOS','sheldon',1,0,15807714,'[]',0,'','ylic,damao,legendyi,oneway','eosc33eosc33','','1、项目名称：邀请金马写文宣传 EOSC 2、项目描述：\n\n1）、由eos原力社区委员会提供相关eosc的宣传素材，委托作者撰写eosc的宣传文章 2）、由eos原力社区委员会评审文章内容，评审通过后由作者通过个人自媒体渠道进行宣传 3）、作者主要个人自媒体渠道如下（通过以下全部渠道进行发布） A、币乎：粉丝数60000+ B、微信群： 8 个免费群，8个收费群，2个超级会员群 C、简书专栏：粉丝数 6000+ D、知乎：关注数690+ E、微博+力场等 4）、宣传文章发布后由原力社区委员会评估作者宣传结果\n\n3、项目功能：\n\n让更多 EOS 用户了解 EOSC 的价值和优势。\n\n4、项目链接：\n\n币乎文章链接：https://bihu.com/article/1409602260 （这个是主要的内容平台） 简书链接：https://www.jianshu.com/p/bdb7928ed65f 知乎链接：https://zhuanlan.zhihu.com/p/92144585 微博链接：https://weibo.com/ttarticle/p/show?id=2309404439351227449578#_0 力场链接：https://lichang.io/articleDetail/963667',0),(19,16,0,'GO BLOCK游戏活动预算申请','13888.0000 EOS','wangheran123',1,0,15914230,'[]',0,'','legendyi,oneway,ylic,damao','eosc33eosc33','','项目名称：GO BLOCK\n项目描述：游戏内用户冲关根据排名每周可瓜分奖池内的奖金，前三周每周向奖金池放7000EOSC，用户充值的70%也会放入奖金池\n\n项目功能：让EOSC有落地应用，社区用户体验更好，奖金更高\n\n开发人员：共计2人，产品1人，技术1人\n\n开发周期：预计2个工作日\n\n开发费用：13888 EOSC 全部用于放入排行榜奖金池给用户瓜分',0),(20,17,0,'DApp红包分享活动申请','50000.0000 EOS','legendyi',0,0,16772133,'[]',0,'','legendyi,damao','oneway,ylic,eosc33eosc33','','#### 为表示春节答谢，为社区小伙伴谋取春节红包福利。由EOSC预算委员主导，秘书处、超级节点方联合，开启DApp红包分享活动。\n#### 活动时间：2020年1月24、25、26（大年三十、初一、初二）晚上8:00\n\n#### 申请数量：50000EOSC\n\n#### 红包DAPP：是一款在钱包里运行的DAPP，发红包时，生成一个类似淘口令的文本，由红包发放者分享出去。将红包转发至微信群、朋友圈。用户复制口令到DAPP，即可领取红包到EOSC账户。',0),(21,18,0,'原力社区微信群管理费用','100000.0000 EOS','dajieshen',0,0,17780044,'[]',1,'','ylic,damao','oneway,eosc33eosc33','legendyi','自2018年6月接触eosforce.io之后，一直关注其生态建设和技术发展，并提出相应的改进建议。2019年8月担任“EOSC原力神秘基地”群主之后，由于原力官方长期没有客服，所以长期为社区新老成员解决问题，通过线上及线下的方式与社区和官方沟通交流。邀请圈内知名人士参与原力社区治理分享等诸多内容。故申请原力社区微信管理费用，此次申请后2020年之内不再申请。审批费用的20%将用于社区成员红包等其他激励活跃度的措施。',0),(22,19,0,'一个测试,顺便申请一下相关操作的费用','200.0000 EOS','xuyapeng',0,0,17787200,'[]',1,'','damao,ylic,legendyi','oneway,eosc33eosc33','','升级budget合约之后,麦子钱包反映提提案出现BUG,现在通过命令提出提案,申请报销下更新合约操作的手续费',0),(45,42,0,'原力社区微信群管理费用申请','0.0000 EOS','blackfans',1,0,18101223,'[]',1,'','ylic,legendyi,eosc33eosc33,damao','oneway','','自2018年6月接触eosforce.io之后，一直关注其生态建设和技术发展，并提出相应的改进建议。2019年8月担任“EOSC原力神秘基地”群主之后，由于原力官方长期没有客服，所以长期为社区新老成员解决问题，通过线上及线下的方式与社区和官方沟通交流。邀请圈内知名人士参与原力社区治理分享等诸多内容。故申请原力社区微信管理费用，审批费用的20%将用于社区成员红包等其他激励活跃度的措施。',0),(76,56,0,'火火钱包联合EOSC宣发活动申请预算','100000.0000 EOS','youbite',1,0,18563921,'[]',1,'','legendyi,damao,eosc33eosc33,ylic','oneway','','火火钱包具备的闪动红包功能，可供用户通过扫描红包海报二维码随机领取代币，很适合项目进行扩大新用户数和用户持币量。此外火火钱包和众多币圈kol、社区、项目有长期宣发合作，可更好的扩大活动宣传效果。\n火火钱包官网：www.huohuo.net\n春节闪动红包活动：https://mp.weixin.qq.com/s/uRy00mejFo2rSqHFMoRVvA',0),(77,57,0,'原力第五届秘书处3月份工资申请','40000.0000 EOS','geekguoeoscc',1,0,18586549,'[]',1,'','legendyi,damao,eosc33eosc33,ylic','oneway','','预算委员会好，我代表原力第五届秘书处申请下2020年3月份的秘书处各成员工资，每人10000eosc，总共40000eosc，具体的名单与接收账号如下：\n原力队长：duizhang\n李建勇： lijianyong\n风听雨： bar\n郭英： geekguoeoscc',0),(78,58,0,'EOSC 与 Pool-X 锁仓质押活动','400000.0000 EOS','ylic',1,0,18735163,'[]',0,'','ylic,legendyi,damao,oneway','','eosc33eosc33','1、项目名称:EOSC 与 Pool-X 锁仓质押活动 2、项目描述:\n1) Pool-X 平台: https://pool-x.io/staking\n2) 预算用途:EOSC 锁仓三个月，基础年化利率 5.24%，参与 Pool-X 锁仓 EOSC 加\n享年化 8%收益。实际年化利率 5.24%+8%。\n3) 锁仓 Pool-X 可以额外获得 Pool-X 平台币 POL。\n3、项目活动参与细则:\n1) 参与活动时间:4 月 13 日 22:00:00~4 月 14 日 22:00:00\n2) 单个用户质押硬顶:50 万 EOSC\n3) 质押总硬顶:2000 万 EOSC\n4) 总加息经费:40 万 EOSC\n5) 参与规则:活动期间，在 Pool-X 平台参与 EOSC 代币锁仓挖矿 90 日 的用户，可 自动视为参与限时加息活动;\n6) 收益分发:\na.基础收益部分:用户参与 staking 的基础收益将由系统每日自动结算并发放至用户的 Pool-X 矿池账户;\nb.补贴收益部分:平台补贴收益将于活动结束后 7 个工作日内统一发放至 Pool-X 矿池 账户;',0),(79,59,0,'链游传奇、全民炮战','500000.0000 EOS','chain.game',0,0,18895937,'[]',0,'','eosc33eosc33,legendyi','oneway,ylic,damao','','项目名称：链游传奇/全民炮战\n项目描述：\n传奇、全名炮战是2款独立App，目前已支持EOS、ETH、IOST，并接入麦子钱包、Mykey钱包，所有公链可互通场景\n链接：https://chaingame.oss-cn-shanghai.aliyuncs.com/download/qmpz.mykey.apk\n\nhttps://chaingame.oss-cn-shanghai.aliyuncs.com/download/cqss2.mykey.apk\n\nChain.Game是一个以优质内容、精品游戏为主打，专注于引进知名IP，立足于正版游戏的区块链版“Steam”游戏平台。以引入传统爆款游戏，引导更多用户拥抱区块链技术为目标，提供流量转换场景，可为各平台、项目做用户活跃及流量激活。Chain.Game由Dimension提供的基于区块链layer-2的中间件框架X-GEN技术作为应用支撑。\n\n项目功能：以引入传统爆款游戏，引导更多用户拥抱区块链技术为目标，提供流量转换场景、可为各平台、项目做用户活动及流量激进。\n开发人员：6人\n开发周期：预计15个工作日（接入公链）\n开发费用：500000EOSC（包含人工成本费用以及服务器维护费）',0),(80,60,0,'','250000.0000 EOS','legendyi',1,0,19168466,'[]',0,'','legendyi,damao,eosc33eosc33,ylic','oneway','','预算委员会好，我代表原力第一届委员会申请下第一届预算委员各成员工资，每人50000eosc，总共250000eosc',0),(81,61,0,'原力|力场节点竞选活动','250000.0000 EOS','lichangeosc',1,0,19620396,'[]',1,'','dajieshen','','','本次活动主要分为三个阶段：预热、开圈、维持热度\n第一阶段内容如下：\n\n1. 预热活动包含：热门文章、快讯、社群推广、AMA互动\n2. 预热内容：力场宣布竞选原力节点，原力入驻力场并开圈等内容展开活动，在彼此社区依次展开活动。\n3. 预计数据：引力区和力场一共80+社群进行传播； 一直播AMA 10万热度；热门文章+快讯 3-5万曝光\n\n更多详情：https://shimo.im/sheets/xYXYjVykg3ggYcW3/MODOC',0);
/*!40000 ALTER TABLE `motions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-09 11:29:13