FROM ubuntu:18.04

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends apt-utils
RUN apt-get install wget -y
RUN apt-get install curl -y
RUN apt-get install mysql-client -y
RUN apt-get install nginx -y



WORKDIR /opt/budget_back
ADD . ./
ADD nginx/nginx.conf /etc/nginx/

RUN /bin/bash ./set_up_nodejs_12.x.sh
RUN apt-get install -y nodejs

RUN npm i -g n
RUN n stable
RUN npm i -g pm2
RUN npm i -g yarn nodemon @babel/cli @babel/core @babel/node ts-node 
RUN npm install




