module.exports = {
  apps : [
      {
        name        : "watch_action_budget",
        script      : "src/watch_service/watch_action.js",
        interpreter : "babel-node",
        watch       : false,
        env: {
          "NODE_ENV": "development",
        },
        env_production : {
           "NODE_ENV"   : "production"
        }
      },
      {
        name        : "budget_web",
        script      : "src/web/index.js",
        interpreter : "babel-node",
        watch       : false,
        env: {
          "NODE_ENV": "development",
          "NODE_PORT"  : 3010
        },
        env_production : {
           "NODE_ENV"   : "production",
            "NODE_PORT"  : 3010
        }
      }
  ]
}