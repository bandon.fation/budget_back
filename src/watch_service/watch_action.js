import {
  consum,
  send_msg
} from '../amq'

import {
  get_type_key
} from '../utils'

import {
  update_or_create_model
} from '../db_manage/models.js'

import models from '../db_manage/models.js'
import Eos from 'eosforcejs'

import config from '../config'

const EOS_INSTANCE = Eos(config.eos_node);

const update_approve_status = async ({lower_bound = -1, scope = ''}) => {
  let params = {
    code: "eosc.budget",
    json: true,
    limit: 1,
    lower_bound,
    scope,
    table: "approvers",
  }
  let data = await EOS_INSTANCE.getTableRows(params);
  let ob = data.rows.find(i => i.id == lower_bound);
  if(!ob){
    return {
      approved: [],
      unapproved: [],
      requested: []
    }
  }
  return ob;
}

const update_motions_by_id = async ({lower_bound = -1, limit = 1, is_confirmed = false}) => {
  let params = {
    code: "eosc.budget",
    json: true,
    limit,
    lower_bound,
    scope: "eosc.budget",
    table: "motions",
  }
  let data = await EOS_INSTANCE.getTableRows(params);
  for(let row of data.rows){
    let ob = {
      is_confirmed,
      trx_id: '',
      approved: '',
      unapproved: '',
      requested: '',
      is_deleted: false
    }
    ob = Object.assign(ob, row);
    ob.motion_id = ob.id ;
    ob.extern_data = JSON.stringify(ob.extern_data);
    delete ob.id ;
    let approvers = await update_approve_status({lower_bound: ob.motion_id, scope: ob.proposer});
    ob.approved = approvers.approved.join(',');
    ob.unapproved = approvers.unapproved.join(',');
    ob.requested = approvers.requested.join(',');
    await update_or_create_model('motions', ['motion_id', 'approve_end_block_num'], ob)
  }
  return data;
}

export const approved_or_not = async (data, is_confirmed) => {
  const motion_apporoved_struct = {
    motion_id   : '',
    action_name : '',
    approver    : '',
    memo        : '',
    is_confirmed: is_confirmed
  }

  motion_apporoved_struct.action_name = data.act.name;
  motion_apporoved_struct.motion_id = data.act.data.id;
  motion_apporoved_struct.approver  = data.act.data.approver;
  motion_apporoved_struct.memo  = data.act.data.memo;

  await update_or_create_model(
      'motion_apporoved', 
      ['motion_id', 'approver', 'action_name'],
      motion_apporoved_struct
    )
}


const plugins = {
  'eosc.budget_montion_id' (item, is_confirmed) {
    let {block_num, block_time} = item;
    update_motions_by_id({lower_bound: item.act.data.montion_id, is_confirmed});
  },
  async 'eosc.budget_unapprove' (item, is_confirmed) {
    await approved_or_not(item, is_confirmed);
    let {block_num, block_time} = item;
    update_motions_by_id({lower_bound: item.act.data.id, is_confirmed});
  },
  async 'eosc.budget_approve' (item, is_confirmed) {
    await approved_or_not(item, is_confirmed);
    let {block_num, block_time} = item;
    update_motions_by_id({lower_bound: item.act.data.id, is_confirmed});
  },
  async 'eosc.budget_propose' (item, is_confirmed) {
    let {block_num, block_time} = item;
    let latest_motion = await models.motions.findAll({order: [['motion_id', 'desc']], limit: 1});
    let latest_id = latest_motion.length ? latest_motion[0].motion_id : 0;

    while(1){
      let data = await update_motions_by_id({lower_bound: latest_id, limit: 10, is_confirmed, block_num, block_time})
      if(!data.more){
        break ;
      }
      latest_id = data.rows.length ? data.rows[ data.rows.length - 1 ].id : latest_id;
    }
    
  }
}


const watch_action_for_service = async (action_res) => {
  for(let item of action_res.actions){
    let _key = `${item.act.account}_${item.act.name}`;
    let ps = [];
    if(plugins[_key]){
      ps.push( plugins[_key](item, action_res.is_confirmed) );
    }
    await Promise.all(ps);
  }
}

console.log('start watch action for budget')
const main = async () => {
    const queue = 'watch_action_for_budget';
    consum({
      'bind_queue': queue,
      'msg_type': 'exchange',
      'exchange_name': 'actions',
      'prefetch': 1,
      async call_back (msg, channel) {
        let msg_str = msg.content.toString();
        let msg_ob = JSON.parse(msg.content.toString());
        try{
          await watch_action_for_service(msg_ob);
          channel.ack(msg);
        }catch(error){
          console.log(error);
          channel.nack(msg, true);
          return ;
        }
        
      }
    });
}

main();