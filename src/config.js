const mode = new Set(['development', 'production']).has(process.env.NODE_ENV) ? process.env.NODE_ENV : 'development';

const config = {
  development: {
    database: {
      host: 'budget_back_mariadb',
      db_name: 'budget_back',
      name: 'root',
      password: '123456'
    },
    database: {
      host: '0.0.0.0',
      db_name: 'budget',
      name: 'root',
      password: 'PASSWORD'
    },
    eos_node: {
      keyProvider: 'wif',
      httpEndpoint: 'http://192.168.1.146:8025',
      chainId: ''
    },
    mq: {
      host: '192.168.1.166',
      port: 5672,
      user: 'bd',
      password: 'bd'
    }
  },
  production: {
    database: {
      host: 'budget_back_mariadb',
      db_name: 'budget_back',
      name: 'root',
      password: '123456'
    },
    eos_node: {
      keyProvider: 'wif',
      httpEndpoint: 'https://w1.eosforce.cn',
      httpEndpoint: 'http://185.170.209.33:9999',
      chainId: ''
    },
    mq: {
      host: '185.170.209.33',
      host: 'localhost',
      host: '172.18.0.1',
      port: 5672,
      user: 'bd',
      password: 'bd'
    }
  }
}

const current_env = config[mode]

// update mq from process env
for(let _key in current_env.mq){
  let config_key = `amq_${_key}`,
      pre_val = current_env.mq[_key];
  let value = typeof process.env[config_key] != 'undefined' ? process.env[config_key] : pre_val;
  if(value){
    current_env.mq[_key] = value
  }
}

export default current_env;
