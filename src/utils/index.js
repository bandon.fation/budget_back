import Eos from 'eosforcejs'

const query_abis = async (accounts, EOS_INSTANCE) => {
  let ps = [];
  for(let i of accounts){
    ps.push( EOS_INSTANCE.getAbi(i) );
  }
  let result = await Promise.all(ps);
  return result;
}

export const get_type_key = async (action, type = 'account_name', EOS_INSTANCE) => {
  let abis = await query_abis([action.act.account], EOS_INSTANCE), 
        {structs, tables, actions} = abis[0].abi,
        action_format = structs.find(i => i.name == action.act.name);
  let account_keys = [], result = [];
  action_format.fields.forEach(item => {
    if(item.type == type){
      result.push( action.act.data[item.name] );
    }
    if(item.type == `${type}[]`){
      result.splice(result.length, 0, ...action.act.data[item.name]);
    }
  });
  result = [...new Set(result)];
  return result;
}

export const wait_time = (_time) => {
  return new Promise((rs, rj) => {
    setTimeout(() => {
      rs();
    }, _time * 1000);
  })
}
